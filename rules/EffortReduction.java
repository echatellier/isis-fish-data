/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2006 - 2014 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package rules;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import scripts.SiMatrix;
import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.Strategy;
import fr.ifremer.isisfish.rule.AbstractRule;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.annotations.Doc;

/**
 * EffortReduction.java
 *
 * Created: 3 septembre 2008
 *
 * @author anonymous &lt;anonymous@labs.libre-entreprise.org&gt;
 * @version $Revision$
 */
public class EffortReduction extends AbstractRule {

    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory.getLog(EffortReduction.class);

    @Doc("Begin step")
    public TimeStep param_beginStep = new TimeStep(0);
    @Doc("End step")
    public TimeStep param_endStep = new TimeStep(59);

    @Doc("Pourcentage de reduction d effort applique.")
    public double param_PercentReduction = 0.5;

    protected boolean first = true;

    protected String[] necessaryResult = {
        // put here all necessary result for this rule
        // example: 
        // MatrixBiomass.NAME,
        // MatrixNetValueOfLandingsPerStrategyMet.NAME
    };

    /**
     * @return the necessaryResult
     */
    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

    /**
     * Permet d'afficher a l'utilisateur une aide sur la regle.
     * @return L'aide ou la description de la regle
     */
    @Override
    public String getDescription() {
        return "Reduce monthly effort of each strategy of the percent indicated";
    }

    /**
     * Appele au demarrage de la simulation, cette methode permet d'initialiser
     * des valeurs.
     * @param context La simulation pour lequel on utilise cette regle
     */
    @Override
    public void init(SimulationContext context) throws Exception {
    }

    /**
     * La condition qui doit etre vrai pour faire les actions.
     * 
     * @param context la simulation pour lequel on utilise cette regle
     * @param step le pas de temps courant
     * @param metier le metier concerné
     * @return vrai si on souhaite que les actions soit faites
     */
    @Override
    public boolean condition(SimulationContext context, TimeStep step, Metier metier)
            throws Exception {

        boolean result = true;
        if (step.before(param_beginStep)) {
            result = false;
        } else if (step.after(param_endStep)) {
            result = false;
        }
        if (result) {
            log.info("condition vraie");
        }
        return result;
    }

    /**
     * Si la condition est vrai alors cette action est executee avant le pas
     * de temps de la simulation.
     * 
     * @param context la simulation pour lequel on utilise cette regle
     * @param step le pas de temps courant
     * @param metier le metier concerné
     */
    @Override
    public void preAction(SimulationContext context, TimeStep step, Metier metier)
            throws Exception {
        // shinte la boucle metier
        if (first) {
            first = false;
            SiMatrix siMatrix = SiMatrix.getSiMatrix(context);
            List<Strategy> strs = siMatrix.getStrategies(step);
            for (Strategy str : strs) {
                log.info("strategy evaluee : " + str.getName());

                /* Dans un premiere temps tant que l inactivité est un entier on utilise 
                * la proportion du nombre de bateaux de la strategie pour reduire l effort
                * ce qu on ferait aussi pour une mesure de reduction du nombre de bateaux mais 
                * comme actuellement on ne tient pas compte de l economie... ca revient au meme
                * En fait passer par l inactivité n est pas la meilleure facon de modifier 
                * l effort le mieux serait d agir sur un autre coeff qui est multiplié a 
                * l effort (Fstd ou ciblage) car comme ca le code serait generique mais on 
                * ne verrait pas que l effort nominal est modifié...
                */

                double propOld = str.getProportionSetOfVessels();
                double newProp = propOld * (1 - param_PercentReduction);
                str.setProportionSetOfVessels(newProp);
            }
        }
    }

    /**
     * Si la condition est vrai alors cette action est executée apres le pas
     * de temps de la simulation.
     * 
     * @param context La simulation pour lequel on utilise cette regle
     * @param step le pas de temps courant
     * @param metier le metier concerné
     */
    @Override
    public void postAction(SimulationContext context, TimeStep step, Metier metier)
            throws Exception {
        first = true;
    }

}
