/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2006 - 2016 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package exports;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Writer;

import org.nuiton.math.matrix.*;

import fr.ifremer.isisfish.entities.*;
import fr.ifremer.isisfish.export.ExportStep;
import fr.ifremer.isisfish.types.TimeStep;
import resultinfos.MatrixBiomass;
import fr.ifremer.isisfish.datastore.SimulationStorage;

/**
 * Biomasses.java
 */
public class Biomasses implements ExportStep {

    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory.getLog(Biomasses.class);

    protected String[] necessaryResult = {
        MatrixBiomass.NAME
    };

    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

    @Override
    public String getExportFilename() {
        return "Biomasses";
    }

    @Override
    public String getExtensionFilename() {
        return ".csv";
    }

    @Override
    public String getDescription() {
        return "Exporte les biomasses tableau avec des lignes step;pop;id;zone;nombre";
    }

    /*@Override
    public void export(SimulationStorage simulation, Writer out) throws Exception {
        for (Population pop : simulation.getParameter().getPopulations()) {
            MatrixND mat = simulation.getResultStorage().getMatrix(pop, MatrixBiomass.NAME);
            for (MatrixIterator i = mat.iterator(); i.hasNext();) {
                i.next();
                Object[] sems = i.getSemanticsCoordinates();
                TimeStep step = (TimeStep) sems[0];
                PopulationGroup group = (PopulationGroup) sems[1];
                Zone zone = (Zone) sems[2];

                double val = i.getValue();
                out.write(pop.getName() + ";" + group.getId() + ";" + zone.getName() + ";" + step.getStep() + ";" + val + "\n");
            }
        }
    }*/

    @Override
    public void exportBegin(SimulationStorage simulation, Writer out) throws Exception {
        out.write("step;population;group;zone;value\n");
    }

    @Override
    public void export(SimulationStorage simulation, TimeStep step, Writer out) throws Exception {
        for (Population pop : simulation.getParameter().getPopulations()) {
            MatrixND mat = simulation.getResultStorage().getMatrix(step, pop, MatrixBiomass.NAME);
            for (MatrixIterator i = mat.iterator(); i.hasNext();) {
                i.next();
                Object[] sems = i.getSemanticsCoordinates();
                PopulationGroup group = (PopulationGroup) sems[0];
                Zone zone = (Zone) sems[1];

                double val = i.getValue();
                out.write(step.getStep() + ";" + pop.getName() + ";" + group.getId() + ";" + zone.getName() + ";" + val + "\n");
            }
        }
        
    }

    @Override
    public void exportEnd(SimulationStorage simulation, Writer out) throws Exception {
        
    }
}
