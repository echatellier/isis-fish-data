/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2009 - 2014 Ifremer, Code Lutin, Jean Couteau, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package sensitivityexports;

import fr.ifremer.isisfish.datastore.ResultStorage;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.export.SensitivityExport;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.annotations.Doc;
import resultinfos.MatrixBiomass;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixIterator;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.TopiaContext;
import org.nuiton.util.ReverseFileReader;

import java.io.File;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SensitivitySpawningBiomassRelativeReferenceY11 implements
        SensitivityExport {

    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory
            .getLog(SensitivitySpawningBiomassRelativeReferenceY11.class);

    protected String[] necessaryResult = {
            MatrixBiomass.NAME
    };

    @Doc("Population")
    public Population param_pop;

    @Doc("URL of the Biomasses.csv reference export file")
    public String param_referenceBiomassCSV = "";

    @Override
    public void export(SimulationStorage simulation, Writer out)
            throws Exception {

        ResultStorage resultStorage = simulation.getResultStorage();
        TimeStep lastStep = resultStorage.getLastStep();
        TimeStep firstStep = new TimeStep(11);
        double biomass = 0.0;
        double firstbiomass = 0.0;
        double biomassReference = 0.0;
        double firstBiomassReference = 0.0;

        File referenceFile = new File(param_referenceBiomassCSV);

        TopiaContext tx = simulation.getStorage().beginTransaction();

        Population tempPop = (Population)tx.findByTopiaId(param_pop.getTopiaId());

        List<PopulationGroup> groups = tempPop.getPopulationGroup();

        Map<Integer, Double> map = new HashMap<>();

        for (PopulationGroup group:groups) {
            map.put(group.getId(),group.getMaturityOgive());
        }

        //instantiate a reader that starts reading from the file end.
        ReverseFileReader reader = new ReverseFileReader(referenceFile);

        //read the last line of the file (should be empty)
        String line = reader.readLine();

        //read the next line (the last written line)
        line = reader.readLine();


        String lastMonth="";

        while (line != null ) {

            String[] items = line.split(";");

            //If we did not initiate the last date (we are on the last line of
            // the file), we do it now with the value of the date.
            if ("".equals(lastMonth)) {
                lastMonth = items[3];
            }

            if ((items[0].equals(param_pop.getName())) && (items[3].equals(lastMonth))){
                biomassReference += Double.parseDouble(items[4]) * map.get(Integer.parseInt(items[1]));
            }

            if ((items[0].equals(param_pop.getName())) && (items[3].equals("11"))){
                firstBiomassReference += Double.parseDouble(items[4]) * map.get(Integer.parseInt(items[1]));
            }

            line = reader.readLine();
        }
        reader.close();

        for (Population pop : simulation.getParameter().getPopulations()) {
            if (pop.getName().equals(param_pop.getName())) {

                //Get the biomass of the first time step
                MatrixND matfirstdate = resultStorage.getMatrix(pop,
                        MatrixBiomass.NAME, tx);
                for (MatrixIterator i = matfirstdate.iterator(); i.hasNext();) {
                    i.next();
                    Object[] sems = i.getSemanticsCoordinates();
                    PopulationGroup group = (PopulationGroup) sems[1];
                    TimeStep step = (TimeStep) sems[0];
                    if (step.equals(firstStep)) {
                        firstbiomass += i.getValue()
                                * group.getMaturityOgive();
                    }
                }

                //Get the biomass of the last time step
                MatrixND matlastdate = resultStorage.getMatrix(pop,
                        MatrixBiomass.NAME, tx);
                for (MatrixIterator i = matlastdate.iterator(); i.hasNext();) {
                    i.next();
                    Object[] sems = i.getSemanticsCoordinates();
                    PopulationGroup group = (PopulationGroup) sems[1];
                    TimeStep step = (TimeStep) sems[0];
                    if (step.equals(lastStep)) {
                        biomass = biomass + i.getValue()
                                * group.getMaturityOgive();
                    }
                }
            }
        }

        tx.closeContext();

        out.write(Double.toString((biomass / firstbiomass)
                / (biomassReference / firstBiomassReference)));
    }

    @Override
    public String getDescription() {
        return "Biomass of the genitors for the last time step. Biomass is the sum on the groups and zones";
    }

    @Override
    public String getExportFilename() {
        return "SensitivitySpawningBiomassRelativeReferenceY11_" + param_pop.getName();
    }

    @Override
    public String getExtensionFilename() {
        return ".csv";
    }

    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

}
