/*
 * Copyright (C) 2018 chatellier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 */
package objectives;

import java.util.List;

import org.nuiton.math.matrix.*;
import resultinfos.*;

import fr.ifremer.isisfish.annotations.Doc;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.entities.*;
import fr.ifremer.isisfish.simulator.Objective;
import fr.ifremer.isisfish.simulator.OptimizationContext;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.datastore.ResultStorage;

/**
 * ObjectiveIterationCount.java
 */
public class ObjectiveIterationCount implements Objective {

    @Doc("Iteration count to perform")
    public int param_iterationCount = 3;
    
    protected String[] necessaryResult = {};

    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

    /**
     * Permet d'afficher a l'utilisateur une aide sur le plan.
     * @return L'aide ou la description du plan
     */
    @Override
    public String getDescription() throws Exception {
        return "Only compute 'n' iteration for associated optimization plan";
    }

    /**
     * Effectue une evaluation entre les exports et les observations.
     * 
     * @param context optimization context
     * @param simulation current simulation
     * @param exports exports
     * @param observations observation
     * @return double value
     */
    public double eval(OptimizationContext context, SimulationStorage simulation, List<MatrixND> exports, List<MatrixND> observations) {
        double result = 0;
        if (context.getCurrentGeneration() == param_iterationCount - 1) { // because starts at 0
            result = -1;
        }
        return result;
    }
}
