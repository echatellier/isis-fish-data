/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2012, 2016 Ifremer, CodeLutin, lgasche
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package scripts;

import fr.ifremer.isisfish.annotations.Nocache;

/**
 * FonctionObjectif_Baranov.java
 *
 * Created: 10 aout 2012
 *
 * @author lgasche &lt;user.name@vcs.hostName&gt;
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@Nocache
public class ObjectiveFunctionBaranov extends ObjectiveFunction {

    protected double C, M, N;
    
    public ObjectiveFunctionBaranov(double C, double M, double N) { // step ??
        this.C = C;
        this.M = M;
        this.N = N;
    }

    public double compute(double xx) {    
        double ff = Math.pow((C - (xx/(xx+M))*(1-Math.exp(-(xx+M)))*N),2);
        return ff;  
    }
}