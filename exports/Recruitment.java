/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2015 - 2017 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package exports;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Writer;

import org.nuiton.math.matrix.*;

import fr.ifremer.isisfish.entities.*;
import fr.ifremer.isisfish.export.ExportStep;
import fr.ifremer.isisfish.types.TimeStep;
import resultinfos.MatrixRecruitment;
import fr.ifremer.isisfish.datastore.SimulationStorage;

/**
 * Recruitment.
 */
public class Recruitment implements ExportStep {

    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory.getLog(Recruitment.class);

    protected String[] necessaryResult = {
        MatrixRecruitment.NAME
    };

    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

    @Override
    public String getExportFilename() {
        return "Recruitment";
    }

    @Override
    public String getExtensionFilename() {
        return ".csv";
    }

    @Override
    public String getDescription() {
        return "Exporte le recrutement par pop par en tableau avec des lignes pop;date;recrutement.";
    }

    /*@Override
    public void export(SimulationStorage simulation, Writer out) throws Exception {
        for (Population pop : simulation.getParameter().getPopulations()) {
            MatrixND mat = simulation.getResultStorage().getMatrix(pop, MatrixRecruitment.NAME);
            for (MatrixIterator i = mat.iterator(); i.hasNext();) {
                i.next();
                Object[] sems = i.getSemanticsCoordinates();
                TimeStep step = (TimeStep) sems[0];

                double val = i.getValue();
                out.write(pop.getName() + ";" + step.getStep() + ";" + val + "\n");
            }
        }
    }*/

    @Override
    public void exportBegin(SimulationStorage simulation, Writer out) throws Exception {
        out.write("population;step;value\n");
    }

    @Override
    public void export(SimulationStorage simulation, TimeStep step, Writer out) throws Exception {
        for (Population pop : simulation.getParameter().getPopulations()) {
            MatrixND mat = simulation.getResultStorage().getMatrix(step, pop, MatrixRecruitment.NAME);
            for (MatrixIterator i = mat.iterator(); i.hasNext();) {
                i.next();

                double val = i.getValue();
                out.write(pop.getName() + ";" + step.getStep() + ";" + val + "\n");
            }
        }
    }

    @Override
    public void exportEnd(SimulationStorage simulation, Writer out) throws Exception {

    }
}
