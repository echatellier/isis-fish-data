/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * #L%
 */
package sensitivityexports;

import java.io.Writer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;

import fr.ifremer.isisfish.datastore.ResultStorage;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.export.SensitivityExport;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.annotations.Doc;
import resultinfos.MatrixLbar;

public class SensitivityLbarJanvierY1 implements SensitivityExport {

    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory.getLog(SensitivityLbarJanvierY1.class);

    protected String[] necessaryResult = {
        MatrixLbar.NAME
    };

    @Doc("Population")
    public Population param_pop;

    @Override
    public void export(SimulationStorage simulation, Writer out)
            throws Exception {
        ResultStorage resultStorage = simulation.getResultStorage();
        TimeStep lastStep = resultStorage.getLastStep();
        TimeStep janvierLastYear = new TimeStep(12 * lastStep.getYear()); 
        double lbar = 0.0;
        for (Population pop : simulation.getParameter().getPopulations()) {
            if (pop.getName().equals(param_pop.getName())) {

                //Get the lbar of the last time step
                MatrixND matlastJan = resultStorage.getMatrix(janvierLastYear, pop,
                        MatrixLbar.NAME);
                MatrixND meanLastJan = matlastJan.meanOverDim(0); // moyenne sur les zones
                lbar = meanLastJan.sumAll(); // On somme tout mais en fait il n'y a plus qu'une valeur dans la matrice; permet d'avoir un double
            }
        }
        out.write(Double.toString(lbar));
    }

    @Override
    public String getDescription() {
        return "lbar for January of the last year. lbar is the mean lbar over zones";
    }

    @Override
    public String getExportFilename() {
        return "SensitivityLbarJanvierY1_" + param_pop.getName();
    }

    @Override
    public String getExtensionFilename() {
        return ".csv";
    }

    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

}
