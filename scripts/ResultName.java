/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2006 - 2015 Ifremer, Code Lutin, Cedric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package scripts;

import fr.ifremer.isisfish.annotations.Doc;
import resultinfos.*;

/**
 * Contient l'ensemble des noms des differents resultats. Le mieux lorsque l'on
 * veut un nouveau resultat est d'ajouter une constante ici, et de l'utiliser
 * ensuite lors de la creation de la matrice.
 * <p>
 * Ceci permet d'avoir un endroit unique ou l'on voit l'ensemble des resultats
 * potentiellement disponible et de ne pas ce tromper en ecrivent le nom d'un
 * resultat
 * <p>
 * Cette classe ne doit contenir que des noms de resultat en static public
 * String l'interface de lancement de simulation se base sur cette classe pour
 * afficher l'ensemble des resultats disponible
 * 
 * @author poussin
 * 
 * @deprecated since 4.4, result has been moved to dedicated classes in "resultname" package.
 *      This class will remaing to ensure compatibility issue with pre 4.4 users scripts
 */
@Deprecated
public class ResultName {

    /**
     * Matrix with 4 dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Metier
     * Dimension 3 : Population
     * Dimension 4 : Zone
     */
    @Doc(value = "do the doc of Result matrixDiscardsWeightPerStrMet")
    static final public String MATRIX_DISCARDS_WEIGHT_PER_STR_MET_PER_ZONE_POP = MatrixDiscardsWeightPerStrMetPerZonePop.NAME;

    /**
     * Matrix with five dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     * Dimension 4 : Group
     * Dimension 5 : Zone
     */
    @Doc(value = "do the doc of Result matrixDiscardsPerStrMet")
    static final public String MATRIX_DISCARDS_PER_STR_MET_PER_ZONE_POP = MatrixDiscardsPerStrMetPerZonePop.NAME;

    /**
     * Matrix with five dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     * Dimension 4 : Group
     * Dimension 5 : Zone
     */
    @Doc(value = "do the doc of Result matrixLandingPerMet")
    static final public String MATRIX_LANDING_PER_MET = MatrixLandingPerMet.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     */
    @Doc(value = "do the doc of Result matrixEffortPerStrategyMet")
    static final public String MATRIX_EFFORT_PER_STRATEGY_MET = MatrixEffortPerStrategyMet.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     */
    @Doc(value = "do the doc of Result matrixEffortNominalPerStrategyMet")
    static final public String MATRIX_EFFORT_NOMINAL_PER_STRATEGY_MET = MatrixEffortNominalPerStrategyMet.NAME;

    /**
     * Matrix with five dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     * Dimension 4 : Group
     * Dimension 5 : Zone
     */
    @Doc(value = "Disponible uniquement avec les simulations par Zone. do the doc of Result matrixCatchRatePerStrategyMet")
    static final public String MATRIX_CATCH_RATE_PER_STRATEGY_MET_PER_ZONE_POP = MatrixCatchRatePerStrategyMet.NAME;

    /**
     * Matrix with five dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     * Dimension 4 : Group
     * Dimension 5 : Zone
     * 
     * N'est calcule que si l'effort est calcule par cellule et non par zone.
     * @see simulators.SimulatorEffortByCell
     */
    @Doc(value = "do the doc of Result matrixCatchPerStrategyMetPerZoneMet")
    static final public String MATRIX_CATCH_PER_STRATEGY_MET_PER_ZONE_MET = MatrixCatchPerStrategyMetPerZoneMet.NAME;

    /**
     * Matrix with five dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     * Dimension 4 : Group
     * Dimension 5 : Zone
     */
    @Doc(value = "do the doc of Result matrixCatchPerStrategyMetPerZonePop")
    static final public String MATRIX_CATCH_PER_STRATEGY_MET_PER_ZONE_POP = MatrixCatchPerStrategyMetPerZonePop.NAME;

    /**
     * Matrix with five dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     * Dimension 4 : Group
     * Dimension 5 : Zone
     * 
     * N'est calcule que si l'effort est calcule par cellule et non par zone.
     * @see simulators.SimulatorEffortByCell
     */
    @Doc(value = "do the doc of Result matrixCatchWeightPerStrategyMetPerZoneMet")
    static final public String MATRIX_CATCH_WEIGHT_PER_STRATEGY_MET_PER_ZONE_MET = MatrixCatchWeightPerStrategyMetPerZoneMet.NAME;

    /**
     * Matrix with five dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     * Dimension 4 : Group
     * Dimension 5 : Zone
     */
    @Doc(value = "do the doc of Result matrixCatchWeightPerStrategyMetPerZonePop")
    static final public String MATRIX_CATCH_WEIGHT_PER_STRATEGY_MET_PER_ZONE_POP = MatrixCatchWeightPerStrategyMetPerZonePop.NAME;

    /**
     * Matrix with five dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     * Dimension 4 : Group
     * Dimension 5 : Zone
     */
    @Doc(value = "Disponible uniquement avec les simulations par Zone. do the doc of Result matrixFishingMortality")
    static final public String MATRIX_FISHING_MORTALITY = MatrixFishingMortality.NAME;

    /**
     * Matrix with 1 dimension
     * Dimension 1 : TimeStep
     */
    @Doc(value = "do the doc of Result matrixTotalFishingMortality")
    static final public String MATRIX_TOTAL_FISHING_MORTALITY = MatrixTotalFishingMortality.NAME;
    
    /**
     * Matrix with 2 dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Group
     */
    @Doc(value = "do the doc of Result matrixFishingMortalityPerGroup")
    static final public String MATRIX_FISHING_MORTALITY_PER_GROUP = MatrixFishingMortalityPerGroup.NAME;
    
    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Group (semantics : Dimension 1)
     * Dimension 3 : Zone (semantics : Dimension 2)
     */
    @Doc(value = "do the doc of Result matrixAbundance")
    static final public String MATRIX_ABUNDANCE = MatrixAbundance.NAME;
    
    /**
     * Matrix with two dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Zone (semantics : Dimension 1)
     */
    @Doc(value = "do the doc of Result matrixLbar")
    static final public String MATRIX_LBAR = MatrixLbar.NAME;
    
    /**
     * Matrix with one dimension
     * Dimension 1 : TimeStep
     */
    @Doc(value = "do the doc of Result matrixRecruitment")
    static final public String MATRIX_RECRUITMENT = MatrixRecruitment.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : Date
     * Dimension 2 : Group
     * Dimension 3 : Zone
     */
    @Doc(value = "do the doc of Result matrixAbundanceBeginMonth")
    static final public String MATRIX_ABUNDANCE_BEGIN_MONTH = MatrixAbundanceBeginMonth.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Group
     * Dimension 3 : Zone
     */
    @Doc(value = "do the doc of Result matrixBiomass")
    static final public String MATRIX_BIOMASS = MatrixBiomass.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Group
     * Dimension 3 : Zone
     */
    @Doc(value = "do the doc of Result matrixBiomassBeginMonth")
    static final public String MATRIX_BIOMASS_BEGIN_MONTH = MatrixBiomassBeginMonth.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     */
    @Doc(value = "do the doc of Result matrixFishingTimePerMonthPerVessel")
    static final public String MATRIX_FISHING_TIME_PER_MONTH_PER_VESSEL = MatrixFishingTimePerMonthPerVessel.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     */
    @Doc(value = "do the doc of Result matrixFuelCostsOfTravelPerVessel")
    static final public String MATRIX_FUEL_COSTS_OF_TRAVEL_PER_VESSEL = MatrixFuelCostsOfTravelPerVessel.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     */
    @Doc(value = "do the doc of Result matrixCostsOfFishingPerVessel")
    static final public String MATRIX_COSTS_OF_FISHING_PER_VESSEL = MatrixCostsOfFishingPerVessel.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     */
    @Doc(value = "do the doc of Result matrixFuelCostsPerVessel")
    static final public String MATRIX_FUEL_COSTS_PER_VESSEL = MatrixFuelCostsPerVessel.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     */
    @Doc(value = "do the doc of Result matrixRepairAndMaintenanceGearCostsPerVessel")
    static final public String MATRIX_REPAIR_AND_MAINTENANCE_GEAR_COSTS_PER_VESSEL = MatrixRepairAndMaintenanceGearCostsPerVessel.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     */
    @Doc(value = "do the doc of Result matrixOtherRunningCostsPerVessel")
    static final public String MATRIX_OTHER_RUNNING_COSTS_PER_VESSEL = MatrixOtherRunningCostsPerVessel.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     */
    @Doc(value = "do the doc of Result matrixSharedNotFixedCostsPerVessel")
    static final public String MATRIX_SHARED_NOT_FIXED_COSTS_PER_VESSEL = MatrixSharedNotFixedCostsPerVessel.NAME;

    /**
     * Matrix with four dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     * Dimension 4 : Population
     */
    @Doc(value = "do the doc of Result matrixGrossValueOfLandingsPerSpeciesPerStrategyMet")
    static final public String MATRIX_GROSS_VALUE_OF_LANDINGS_PER_SPECIES_PER_STRATEGY_MET = MatrixGrossValueOfLandingsPerSpeciesPerStrategyMet.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     */
    @Doc(value = "do the doc of Result matrixGrossValueOfLandingsPerStrategyMet")
    static final public String MATRIX_GROSS_VALUE_OF_LANDINGS_PER_STRATEGY_MET = MatrixGrossValueOfLandingsPerStrategyMet.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     *
     * Matrix calculated in the non generic Rule : GraviteVPUE1LangEtGrossValueOtherSpeciesECOMOD
     */
    @Doc(value = "do the doc of Result matrixGrossValueOfLandingsOtherSpeciesPerStrategyMet")
    static final public String MATRIX_GROSS_VALUE_OF_LANDINGS_OTHER_SPECIES_PER_STRATEGY_MET = MatrixGrossValueOfLandingsOtherSpeciesPerStrategyMet.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     */
    @Doc(value = "do the doc of Result matrixGrossValueOfLandingsPerStrategyMetPerVessel")
    static final public String MATRIX_GROSS_VALUE_OF_LANDINGS_PER_STRATEGY_MET_PER_VESSEL = MatrixGrossValueOfLandingsPerStrategyMetPerVessel.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     */
    @Doc(value = "do the doc of Result matrixNetValueOfLandingsPerStrategyMet")
    static final public String MATRIX_NET_VALUE_OF_LANDINGS_PER_STRATEGY_MET = MatrixNetValueOfLandingsPerStrategyMet.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     */
    @Doc(value = "do the doc of Result matrixNetValueOfLandingsPerStrategyMetPerVessel")
    static final public String MATRIX_NET_VALUE_OF_LANDINGS_PER_STRATEGY_MET_PER_VESSEL = MatrixNetValueOfLandingsPerStrategyMetPerVessel.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     */
    @Doc(value = "do the doc of Result matrixNetRenevueToSharePerStrategyMetPerVessel")
    static final public String MATRIX_NET_RENEVUE_TO_SHARE_PER_STRATEGY_MET_PER_VESSEL = MatrixNetRevenueToSharePerStrategyMetPerVessel.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     */
    @Doc(value = "do the doc of Result matrixCrewSharePerStrategyPerVessel")
    static final public String MATRIX_CREW_SHARE_PER_STRATEGY_MET_PER_VESSEL = MatrixCrewSharePerStrategyPerVessel.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     */
    @Doc(value = "do the doc of Result matrixOwnerMarginOverVariableCostsPerStrategyMetPerVessel")
    static final public String MATRIX_OWNER_MARGIN_OVER_VARIABLE_COSTS_PER_STRATEGY_MET_PER_VESSEL = MatrixOwnerMarginOverVariableCostsPerStrategyMetPerVessel.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     */
    @Doc(value = "do the doc of Result matrixVesselMarginOverVariableCostsPerStrategyMetPerVessel")
    static final public String MATRIX_VESSEL_MARGIN_OVER_VARIABLE_COSTS_PER_STRATEGY_MET_PER_VESSEL = MatrixVesselMarginOverVariableCostsPerStrategyMetPerVessel.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     */
    @Doc(value = "do the doc of Result matrixOwnerMarginOverVariableCostsPerStrategyPerVessel")
    static final public String MATRIX_OWNER_MARGIN_OVER_VARIABLE_COSTS_PER_STRATEGY_PER_VESSEL = MatrixOwnerMarginOverVariableCostsPerStrategyPerVessel.NAME;

    /**
     * Matrix with two dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     */
    @Doc(value = "do the doc of Result matrixOwnerMarginOverVariableCostsPerStrategy")
    static final public String MATRIX_OWNER_MARGIN_OVER_VARIABLE_COSTS_PER_STRATEGY = MatrixOwnerMarginOverVariableCostsPerStrategy.NAME;

    /**
     * Matrix with two dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     */
    @Doc(value = "do the doc of Result matrixVesselMarginOverVariableCostsPerStrategyPerVessel")
    static final public String MATRIX_VESSEL_MARGIN_OVER_VARIABLE_COSTS_PER_STRATEGY_PER_VESSEL = MatrixVesselMarginOverVariableCostsPerStrategyPerVessel.NAME;

    /**
     * Matrix with two dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     */
    @Doc(value = "do the doc of Result matrixVesselMarginOverVariableCostsPerStrategy")
    static final public String MATRIX_VESSEL_MARGIN_OVER_VARIABLE_COSTS_PER_STRATEGY = MatrixVesselMarginOverVariableCostsPerStrategy.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Strategy
     * Dimension 3 : Metier
     */
    @Doc(value = "do the doc of Result matrixNoActivity")
    static final public String MATRIX_NO_ACTIVITY = MatrixNoActivity.NAME;

    /**
     * Matrix with three dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Metier
     * Dimension 3 : Zone
     */
    @Doc(value = "do the doc of Result matrixMetierZone")
    static final public String MATRIX_METIER_ZONE = MatrixMetierZone.NAME;

    /**
     * Matrix with two dimensions
     * Dimension 1 : TimeStep
     * Dimension 2 : Group
     */
    @Doc(value = "do the doc of Result matrixPrice")
    static final public String MATRIX_PRICE = MatrixPrice.NAME;
}
