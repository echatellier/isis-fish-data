/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2015 Ifremer, Code Lutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package resultinfos;

import fr.ifremer.isisfish.result.AbstractResultInfo;

/**
 * Matrix with three dimensions
 * Dimension 1 : TimeStep
 * Dimension 2 : Strategy
 * Dimension 3 : Metier
 */
public class MatrixRepairAndMaintenanceGearCostsPerVessel extends AbstractResultInfo {

    public static final String NAME = "MatrixRepairAndMaintenanceGearCostsPerVessel";

    @Override
    public String getDescription() {
        return "do the doc of Result MatrixRepairAndMaintenanceGearCostsPerVessel";
    }
}
