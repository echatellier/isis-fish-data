/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2009 - 2014 Ifremer, Code Lutin, Jean Couteau
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package sensitivityanalysis;

import java.io.File;
import java.util.List;

import org.nuiton.j2r.REngine;

import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.export.SensitivityExport;
import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.simulator.sensitivity.*;
import fr.ifremer.isisfish.annotations.Doc;

import org.nuiton.j2r.types.RDataFrame;

/**
 * Implementation of Optimum Latin Hypercube method using R.
 * 
 * @author jcouteau
 * @version $Revision: 3842 $
 */
public class OptimumLHS extends AbstractSensitivityAnalysis {

    @Doc("Number of simulations (default=10)")
    public int param_simulationNumber = 10;
    @Doc("The maximum number of times the Columnwise Pairwise algorithm is applied to all the columns(default=2).")
    public int param_MaxSweeps = 2;
    @Doc("The optimal stopping criterion (between 0 and 1) (default=0.1).")
    public double param_eps = 0.1;
    @Doc("True to be able to modify the code sent to R")
    public boolean param_modifR = false;

    /**
     * Retourne vrai si le calculateur sait gerer la cardinalité des facteurs
     * continue.
     * 
     * @return {@code true} s'il sait la gerer
     */
    @Override
    public boolean canManageCardinality() {
        return true;
    }

    @Override
    public SensitivityScenarios compute(DesignPlan plan, File outputDirectory)
            throws SensitivityException {

        setIsisFactorsR(plan, outputDirectory);

        int factorNumber = plan.getFactors().size();
        List<Factor> factors = plan.getFactors();
        RDataFrame dataFrame;
        SensitivityScenarios thisExperiment = new SensitivityScenarios();
        List<Scenario> thisExperimentScenarios = thisExperiment.getScenarios();

        //Test all factors, if one is discrete, return null
        checkAllFactorContinuous(factors);

        try {
            REngine engine = openEngine(outputDirectory);

            //Load the lhs library
            engine.voidEval("library(lhs)");

            String rInstruction = "isis.methodAnalyse<-optimumLHS(n=%s,k=%s,maxSweeps=%s,eps=%s)";

            String rCall = String.format(rInstruction, param_simulationNumber,
                    factorNumber, param_MaxSweeps, param_eps);

            if (param_modifR) {
                rCall = editRInstruction(rCall);
            }

            //Create the scenarios
            engine.voidEval(rCall);

            // Get back experiment plan
            engine.eval("expPlan<-as.data.frame(isis.methodAnalyse)");
            dataFrame = (RDataFrame)engine.eval("expPlan");
            dataFrame.setVariable("expPlan");

            // Setting up the scenarios.
            for (int j = 0; j < param_simulationNumber; j++) {
                Scenario experimentScenario = new Scenario();
                for (int i = 0; i < factorNumber; i++) {
                    Factor factor = plan.getFactors().get(i);
                    factor.setValueForIdentifier(dataFrame.get(i, j));
                    experimentScenario.addFactor(factor);
                }
                thisExperimentScenarios.add(experimentScenario);
                thisExperiment.setScenarios(thisExperimentScenarios);
            }

            engine.voidEval(getIsisFactorDistribution(factors));
            
            engine.voidEval("call<-" + "\"isis.methodAnalyse<-optimumLHS("
                + "n=" + param_simulationNumber
                + ",k=" + factorNumber
                + ",maxSweeps=" + param_MaxSweeps
                + ",eps=" + param_eps + ")\"");

            engine.voidEval("isis.methodExp<-list(" +
                "\"isis.factors\"=isis.factors," +
                "\"isis.factor.distribution\"=isis.factor.distribution," +
                "\"call\"=call)");

            engine.voidEval("attr(isis.methodExp," +
                "\"nomModel\")<-\"isis-fish-externe-R\"");

            engine.voidEval("isis.simule<-data.frame(isis.methodAnalyse)");

            engine.voidEval("attr(isis.simule," +
                "\"nomModel\")<-\"isis-fish-externe-R\"");
            
            engine.voidEval("names(isis.simule)<-isis.factors[[1]]");

            String data = "data<-data.frame(";

            //Create the factors vectors and the dataFrame instruction
            for (int j = 0; j < factors.size(); j++) {
                //The factor values vector
                String vector;
                //Get back the factor
                Scenario scenario = thisExperimentScenarios.get(0);
                Factor factor = scenario.getFactors().get(j);
                String factorName = factor.getName().replaceAll(" ", "");

                vector = factorName + "<-c(";
                for (int i = 0; i < param_simulationNumber; i++) {
                    //Get back the displayed value the factor
                    Scenario tempScenario = thisExperimentScenarios.get(i);
                    Factor tempFactor = tempScenario.getFactors().get(j);
                    Object value = tempFactor.getDisplayedValue();

                    if (i < (param_simulationNumber - 1)) {
                        vector = vector + value + ",";
                    } else {
                        vector += value;
                    }

                }
                vector += ")";
                engine.voidEval(vector);

                if (j < factors.size() - 1) {
                    data += factorName + "=factor(" + factorName + "),";
                } else {
                    data += factorName + "=factor(" + factorName + "))";
                }
            }
            engine.voidEval(data);

            // Creating the factors vector.
            rInstruction = "factornames<-c(";
            for (int i = 0; i < factorNumber; i++) {
                if (i != (factorNumber - 1)) {
                    rInstruction = rInstruction + "\""
                            + factors.get(i).getName() + "\",";
                } else {
                    rInstruction = rInstruction + "\""
                            + factors.get(i).getName() + "\"";
                }
            }

            rInstruction += ")";

            engine.voidEval(rInstruction);

            closeEngine(engine, outputDirectory);

        } catch (Exception e) {
            throw new SensitivityException("Can't generate scenarios", e);
        }

        return thisExperiment;
    }

    @Override
    public void analyzeResult(List<SimulationStorage> simulationStorages,
            File outputDirectory) throws SensitivityException {

        //The first storage to get the name and parameters all along the method
        SimulationStorage storage = simulationStorages.get(0);
        String simulationName = outputDirectory.getName().replaceAll("-", "");

        try {

            REngine engine = openEngine(outputDirectory);

            engine.voidEval("factors<-data.frame(isis.methodAnalyse)");
            engine.voidEval("names(factors)<-isis.factor.distribution$NomFacteur");

            //Get back the factors number
            int factorNumber = (Integer) engine.eval("length(factors[1,])");
            //Get back the simulation number
            param_simulationNumber = (Integer) (engine.eval("length(factors[,1])"));

            SimulationParameter param = storage.getParameter();
            int sensitivityNumber = param.getSensitivityExport().size();

            for (int k = 0; k < sensitivityNumber; k++) {

                SensitivityExport sensitivityExport =
                        param.getSensitivityExport().get(k);

                String rInstruction = createImportInstruction(sensitivityExport,
                        simulationStorages);

                // Send the simulation results
                engine.voidEval(rInstruction);

                //Put results in isis.simule
                engine.voidEval("isis.simule<-data.frame(isis.simule," +
                        sensitivityExport.getExportFilename() + ")");
            }

            //adding attribute to isis.Simule
            engine.voidEval("attr(isis.simule," +
                "\"nomModel\")<-\"isis-fish-externe-R\"");
            engine.voidEval("attr(isis.simule,\"call\")<-isis.methodExp$call");

            for (int k = 0; k < sensitivityNumber; k++) {

                // Creates the R expression to import results in R
                SensitivityExport export = param.getSensitivityExport().get(k);
                String name = export.getExportFilename();

                //Create the dataforaov data.frame
                String dataframe = "dataforaov<-data.frame(factors," +
                    name + "=" + name + ")";
                engine.voidEval(dataframe);

                //Call aov()
                String aovCall = "aovresult<-aov(" + name + "~";
                for (int j = 0; j < factorNumber; j++) {

                    String factorName = (String) engine.eval("names(factors)[" + (j + 1) + "]");
                    factorName = factorName.replaceAll(" ", ".");

                    if (j < (factorNumber - 1)) {
                        aovCall += factorName +
                            "+";
                    } else {
                        aovCall += factorName + ",data=dataforaov)";
                    }
                }
                engine.voidEval(aovCall);

                /*Export the results
                 *Export format is csv, data separated by ','
                 *Results Export name is sensitivityExportName_Results.csv
                 *Sensitivity Indices export name is sensitivityExportName_SensitivityIndices.csv
                 */

                //Compute Sum of Squares and Sensitivity indices
                engine.voidEval("SoS<-summary(aovresult)[[1]][1:dim(summary(aovresult)[[1]])[1],2]");
                engine.voidEval("names(SoS)<-dimnames(summary(aovresult)[[1]])[[1]][1:dim(summary(aovresult)[[1]])[1]]");
                engine.voidEval("IndSensibilite<-SoS/sum(SoS)");

                //Create a data.frame to export sensitivity important results in one file.
                engine.voidEval("exportsensitivity<-data.frame(" +
                        "SoS[1:dim(summary(aovresult)[[1]])[1]]," +
                        "IndSensibilite[1:dim(summary(aovresult)[[1]])[1]])");
                engine.voidEval("names(exportsensitivity)<-c(" +
                        "\"Sum Of Squares\"," +
                        "\"Sensitivity indices\")");
                engine.voidEval("row.names(exportsensitivity)<-dimnames(summary(aovresult)[[1]])[1][[1]][1:dim(summary(aovresult)[[1]])[1]]");


                //Set dataforaov names
                engine.voidEval("names(dataforaov)<-c(isis.factor.distribution$NomFacteur,\"Result\")");

                /*Set the export directory
                 *Export directory is the first simulation export directory. 
                 */
                engine.setwd(outputDirectory);

                //Save the results with the scenarios.
                engine.voidEval("write.csv(dataforaov,\"" +
                        name + "_Results.csv\")");

                //Save the sensitivity indices
                engine.voidEval("write.csv(exportsensitivity,\"" +
                        name + "_SensitivityIndices.csv\")");
                //FIXME export through java to enable export when using Rserve (when distant Rserve).

                //creating isis.methodAnalyse
                String exportMethodAnalyse = String.format("%s.isis.methodAnalyse", simulationName + "." + name);
                engine.voidEval(exportMethodAnalyse + "<-list(" +
                    "\"isis.factors\"=isis.factors," +
                    "\"isis.factor.distribution\"=isis.factor.distribution," +
                    "\"isis.simule\"=isis.simule," +
                    "call_method=\"" + aovCall + "\""+
                    ",\"analysis_result\"=list(aovresult,IndSensibilite))");

                engine.voidEval("attr(" + exportMethodAnalyse + "," +
                    "\"nomModel\")<-\"isis-fish-externe-R\"");

            }

            closeEngine(engine, outputDirectory);

        } catch (Exception e) {
            throw new SensitivityException("Can't evaluate results", e);
        }

    }

    @Override
    public String getDescription() {
        return "Implementation of Random Latin Hypercube method method " +
            "using R (needs 'lhs' package to work)";
    }

}
