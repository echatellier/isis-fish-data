/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2006 - 2015 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package rules;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;

import scripts.SiMatrix;
import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.Strategy;
import fr.ifremer.isisfish.entities.StrategyMonthInfo;
import fr.ifremer.isisfish.rule.AbstractRule;
import fr.ifremer.isisfish.simulator.MetierMonitor;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.types.TimeStep;
import resultinfos.MatrixNoActivity;
import fr.ifremer.isisfish.types.Month;

/**
 * Remplace aussi Cantonnement Engin.
 * 
 * Created: 29 aout 2007
 * @author sigrid
 * @version $Revision$
 */
public class TotalClosure extends AbstractRule {
    
    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory.getLog(TotalClosure.class);

    public TimeStep param_beginStep = new TimeStep(100);
    public TimeStep param_endStep = new TimeStep(119);
    public Month param_beginMonth = Month.JANUARY;
    public Month param_endMonth = Month.JANUARY;

    protected String[] necessaryResult = {
        MatrixNoActivity.NAME
    };

    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

    @Override
    public String getDescription() throws Exception {
        return "Total closure of the fishery";
    }

    /**
    * Appelé au démarrage de la simulation, cette methode permet d'initialiser des valeurs.
    * @param context La simulation pour lequel on utilise cette regle
    */
    @Override
    public void init(SimulationContext context) throws Exception {

    }

    /**
     * La condition qui doit etre vrai pour faire les actions.
     * 
     * @param context la simulation pour lequel on utilise cette regle
     * @param step le pas de temps courant
     * @param metier le metier concerné
     * @return vrai si on souhaite que les actions soit faites
     */
    @Override
    public boolean condition(SimulationContext context, TimeStep step, Metier metier)
            throws Exception {
        boolean result = true;
        if (step.before(param_beginStep)) {
            result = false;
        } else if (step.after(param_endStep)) {
            result = false;
        }

        if (step.getMonth().before(param_beginMonth)) {
            result = false;
        } else if (step.getMonth().after(param_endMonth)) {
            result = false;
        }
        return result;
    }

    /**
     * Si la condition est vrai alors cette action est executee avant le pas
     * de temps de la simulation.
     * 
     * @param context la simulation pour lequel on utilise cette regle
     * @param step le pas de temps courant
     * @param metier le metier concerné
     */
    @Override
    public void preAction(SimulationContext context, TimeStep step, Metier metier)
            throws Exception {
        MetierMonitor metierMon = context.getMetierMonitor();

        SiMatrix siMatrix = SiMatrix.getSiMatrix(context);
        MatrixND noActivity = metierMon.getOrCreateNoActivity(step,
                MatrixNoActivity.NAME, siMatrix.getStrategies(step),
                siMatrix.getMetiers(step));
        metierMon.addforbiddenMetier(metier);
        List<Strategy> strategies = siMatrix.getStrategies(step);

        for (Strategy str : strategies) {
            StrategyMonthInfo info = str.getStrategyMonthInfo(step.getMonth());
            double prop = info.getProportionMetier(metier);
            if (prop != 0) {
                noActivity.setValue(str, metier, prop);
                info.setProportionMetier(metier, 0);
            }
        }
    }

    /**
     * Si la condition est vrai alors cette action est executée apres le pas
     * de temps de la simulation.
     * 
     * @param context La simulation pour lequel on utilise cette regle
     * @param step le pas de temps courant
     * @param metier le metier concerné
     */
    @Override
    public void postAction(SimulationContext context, TimeStep step, Metier metier)
            throws Exception {
        // nothing
    }

}
