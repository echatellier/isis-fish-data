/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2006 - 2016 Ifremer, CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package scripts;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;

import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.Species;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.types.TimeStep;

/**
 * RuleUtil.
 *
 * Created: 6 septembre 2006
 *
 * @author anonymous &lt;anonymous@labs.libre-entreprise.org&gt;
 * @version $Revision: 1.3 $
 *
 * Last update: $Date: 2007-11-20 15:51:05 $
 * by : $Author: bpoussin $
 */
public class RuleUtil {

    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory.getLog(RuleUtil.class);

    /**
     * @param context le context de simulation
     * @param species l'espece sur lequel on souhaite le total
     * @param step le pas de temps pour lequel on veut le calcul, cet argument est
     * passe seulement pour que le cache ne retourne pas toujours la meme valeur
     * @return total catch in tons
     */
    public static double getTotalCatchTons(SimulationContext context, Species species, TimeStep step) {
        double result = 0;
        for (Population pop : species.getPopulation()) {
            MatrixND mat = context.getPopulationMonitor().getHoldCatch(pop);
            if (mat != null) {
                mat = mat.copy();
                mat = mat.sumOverDim(0); // sum over Strategies
                mat = mat.sumOverDim(1); // sum over metiers
                mat = mat.sumOverDim(3); // sum over zones

                List<PopulationGroup> groups = pop.getPopulationGroup();

                for (int c = 0; c < groups.size(); c++) {
                    PopulationGroup group = groups.get(c);
                    double weight = group.getMeanWeight();
                    result += mat.getValue(0, 0, c, 0) * weight / 1000.0;
                }
            }
        }
        return result;
    }
    
    public static double getTotalCatchTonsPop(SimulationContext context, Population pop, TimeStep step) {
        double result = 0;
            MatrixND mat = context.getPopulationMonitor().getHoldCatch(pop);
            if (mat != null) {
                mat = mat.copy();
                mat = mat.sumOverDim(0); // sum over Strategies
                mat = mat.sumOverDim(1); // sum over metiers
                mat = mat.sumOverDim(3); // sum over zones

                List<PopulationGroup> groups = pop.getPopulationGroup();

                for (int c = 0; c < groups.size(); c++) {
                    PopulationGroup group = groups.get(c);
                    double weight = group.getMeanWeight();
                    result += mat.getValue(0, 0, c, 0) * weight / 1000.0;
                }
            }
        return result;
    }
}
